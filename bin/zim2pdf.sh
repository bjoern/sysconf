#!/bin/bash

#call zim2pdf.sh %p %n %s

# example:
#zim --export --output=report.html --format=html --template=myPrint Nextcloud "Journal:2020:Week 50" && weasyprint report.html report.pdf && evince report.pdf

page=$1
notebook=$(basename $2)
filename=$(basename $3 .txt)

## Debug log
#echo $page >> /tmp/log.zim
#echo "****" >> /tmp/log.zim
#echo $notebook >> /tmp/log.zim
#echo "****" >> /tmp/log.zim
#echo $filename >> /tmp/log.zim

cd /tmp 
zim --export --overwrite --output=/tmp/$filename.html --format=html --template=myPrint $notebook "$page" && weasyprint /tmp/$filename.html $filename.pdf && evince $filename.pdf
